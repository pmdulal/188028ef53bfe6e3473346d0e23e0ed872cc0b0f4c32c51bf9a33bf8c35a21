<?php namespace App;

use Carbon\Carbon;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract {

	use Authenticatable, CanResetPassword;

	protected $table = 'users';

	protected $fillable = ['fname', 'email', 'password','punch_in','punch_out'];

	protected $hidden = ['password', 'remember_token'];

    public function userReward()
    {
        return $this->belongsTo('App\SafalSewa\Models\UserReward','id','user_id');
    }

    public function notification()
    {
        return $this->hasMany('App\SafalSewa\Models\Notification','user_id','id');
    }

    public function lastPunchIn()
    {
        $lastLogin = Carbon::parse($this->punch_out);

        $date = '<i class="fa fa-calendar"></i> '.$lastLogin->format('l, j F ');
        $time = '    <i class="fa fa-clock-o"></i> '.$lastLogin->format('h:i A');

        return $date.$time;
    }

}
