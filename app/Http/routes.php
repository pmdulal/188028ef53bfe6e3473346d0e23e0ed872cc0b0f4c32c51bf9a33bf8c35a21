<?php 

Route::get("login",[
    "as" => "loginhere",
    "uses" => "HomeController@login"
]);

Route::post("login",[
    "as" => "doLogin",
    "uses" => "LoginController@checkLogin"
]);

Route::post("register",[
    "as" => "doRegister",
    "uses" => "LoginController@doRegister"
]);

Route::get("info",[
    "middleware" => "auth",
    "as" => "detail.info",
    "uses" => "HomeController@userInfo"
]);

Route::post("imageupload",[
    "middleware" => "auth",
    "as" => "image.upload",
    "uses" => "UploadController@uploadImage"
]);

Route::post("saveDetail",[
    "middleware" => "auth",
    "as" => "info",
    "uses" => "LoginController@insertDetail"
]);

Route::get("logout",[
    "middleware" => "auth",
    "as" => "logout",
    "uses" => "LoginController@logOut"
]);

Route::get("dashboard",[
    "middleware" => "auth",
    "as" => "dashboard",
    "uses" => "HomeController@home"
]);

Route::get("loadfund",[
    "middleware" => "auth",
    "as" => "load.fund",
    "uses" => "HomeController@loadfund"
]);

Route::get("fundtransfer",[
    "middleware" => "auth",
    "as" => "fund.transfer",
    "uses" => "HomeController@fundtransfer"
]);

Route::post("loadingfund",[
    "middleware" => "auth",
    "as" => "fund.fetch",
    "uses" => "TransactionController@loadFund"
]);

Route::post("transferingfund",[
    "middleware" => "auth",
    "as" => "transfer.fund",
    "uses" => "TransactionController@transferFund"
]);

Route::get("ntc/postpaid",[
    "middleware" => "auth",
    "as" => "ntc.postpaid",
    "uses" => "HomeController@ntcpostpaid"
]);

Route::post("ntc/postpaid",[
    "middleware" => "auth",
    "as" => "ntc.postpaid.recharge",
    "uses" => "TransactionController@ntcpostpaidrecharge"
]);

Route::get("ntc/prepaid",[
    "middleware" => "auth",
    "as" => "ntc.prepaid",
    "uses" => "HomeController@ntcprepaid"
]);
//=------

Route::get("test",[
    "middleware" => "auth",
    "as" => "test",
    "uses" => "TransactionController@test"
]);

Route::get("view-notification/{id}",[
    "middleware" => "auth",
    "as" => "view.notification",
    "uses" => "HomeController@viewnotification"
]);