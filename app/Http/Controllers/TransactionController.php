<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\NtcPostPaidFormRequest;
use App\Http\Requests\LoadFundFormRequest;
use App\Http\Controllers\Controller;

use App\SafalSewa\Models\Notification;
use App\SafalSewa\Models\reward;
use App\SafalSewa\Models\Transaction;
use App\User;
use Illuminate\Http\Request;
use Auth;
use DB;

class TransactionController extends Controller
{

	public function index()
	{
		//
	}

    public function ntcpostpaidrecharge(NtcPostPaidFormRequest $request)
    {
//        dd($request->all());
        $controller=$this;
        try{
            DB::transaction(function() use ($request,$controller){
                $data=[
                    'number' => $request->number,
                    'amount' => $request->amount,
                    'user_id' => Auth::id()
                ];

                $transaction = Transaction::create($data);

                if(!$transaction):
                    throw new \Exception('Transaction failed.');
                endif;

                $user = Auth::user();

                if($user->main_balance < $request->amount){
                    throw new \Exception('You do not have sufficient balance in your account. Please load fund.');
                }

                $user->main_balance -= $request->amount;
                $user->total_transaction += $request->amount;

                if($user->reward_point_transaction==0):
                    if($user->total_transaction >= 1000):
                        $user->reward_point_transaction += floor($user->total_transaction / 1000);
                    endif;
                else:
                    if($user->total_transaction >= ($user->reward_point_transaction*1000+1000)):
                        $user->reward_point_transaction += floor(($user->total_transaction/1000) - $user->reward_point_transaction);
                    endif;
                endif;



                if( !$user->save() ):
                    throw new \Exception('Unfortunately transfered failed. Sorry for inconvineince.');
                endif;

                $reward=$controller->test();
//                dd($reward->transfer_point,$user->reward_point_transfer);
                if($user->reward_point_transaction>=$reward->transaction_point && $user->reward_point_transfer>=$reward->transfer_point ):
//                    dd($reward->reward);
                    Notification::create([
                        'description' => 'Rewarded with Rs.'.$reward->reward ,
                        'user_id' => Auth::id()
                    ]);
                endif;

            });

        }catch(\Exception $ex){
            $message = $ex->getMessage();

            return redirect()->route('ntc.postpaid')->with('danger',$message);
        }

        return redirect()->route('ntc.postpaid')->with('success','Successfully transfered');
    }

    public function loadFund(LoadFundFormRequest $request)
    {
        try{
            DB::transaction(function() use ($request)
            {
                $data = [
                    'bank' => $request->bank_name,
                    'ac_number' => $request->account_number,
                    'amount' => $request->amount,
                    'user_id' => Auth::id(),
                ];

                $transaction = Transaction::create($data);

                if( !$transaction ):
                    throw new \Exception('Transaction failed.');
                endif;

                $user = Auth::user();
                $user->main_balance += $request->amount;

                if( !$user->save() ):
                    throw new \Exception('User account balance could not be updated.');
                endif;
            });
        }catch(\Exception $e){
            $message = $e->getMessage();
            return redirect()->route('load.fund')->with('danger',$message);
        }

        return redirect()->route('load.fund')->with('success','You have loaded account balance successfully');
    }

    public function transferFund(Request $request)
    {
        $controller=$this;
        try{
            DB::transaction(function () use($request,$controller){
                $data = [
                    'id_from' => User::find(Auth::id())->unique_key ,
                    'id_to' => $request->Safal_Sewa_Id ,
//                    'user_id' => 1,

                    'amount' => $request->amount,
                    'user_id' => Auth::id()
                ];

                $transaction = Transaction::create($data);
                if( !$transaction ):
                    throw new \Exception('Transaction failed.');
                endif;
//dd(DB::table('users')->where('user_id',$request->get('Safal_Sewa_Id'))->get());
                $user = Auth::user();
//                $controller->checkRewardCriteria();
                if($user->user_id == $request->get('Safal_Sewa_Id')):
                    throw new \Exception('Transaction cannot be done within your account');
                endif;

                if($user->main_balance < $request->amount){
                    throw new \Exception('You do not have sufficient balance in your account. Please load fund.');
                }


                $user->main_balance -= $request->amount;
                $user->total_transfer += $request->amount;
//dd(floor($user->total_transfer/500));
//                if($user->total_transfer >= 500):
//                    $user->reward_point_referal += floor($user->total_transfer/500);
//                endif;
//                ascaasasa
                if($user->reward_point_transfer==0):
                    if($user->total_transfer >= 500):
                        $user->reward_point_transfer += floor($user->total_transfer / 500);
                    endif;
                else:
//                    dd($user->total_transfer,$user->reward_point_transfer*500+500);
                    if($user->total_transfer >= ($user->reward_point_transfer*500+500)):
//                        dd($user->total_transfer/500,$user->reward_point_referal);
                        $user->reward_point_transfer += floor(($user->total_transfer/500) - $user->reward_point_transfer);
//                        dd($user->reward_point_transfer);
                    endif;
                endif;
//                asfasdasd

                if( !$user->save() ):
                    throw new \Exception('Unfortunately transfered failed. Sorry for inconvineince.');
                endif;



                $success = DB::table('users')->where('unique_key',$request->get('Safal_Sewa_Id'))->increment('main_balance',$request->amount);

//                if( !$success ):
//                    throw new \Exception('The Fund Could not be transfered');
//                endif;

                $reward=$controller->test();
//                dd($reward->transfer_point,$user->reward_point_transfer);
                if($user->reward_point_transfer>=$reward->transfer_point && $user->reward_point_transaction>=$reward->transaction_point ):
//                    dd($reward->reward);
                    Notification::create([
                        'description' => 'Rewarded with Rs.'.$reward->reward ,
                        'user_id' => Auth::id()
                    ]);
                endif;

//                checkRewardCriteria($user);



            });

            return redirect()->route('fund.transfer')
                ->with('success','You have successfully transfered Rs. '.$request->amount.' to '.$request->Safal_Sewa_Id);
        }catch(\Exception $ex){
            $message = $ex->getMessage();
            return redirect()->route('fund.transfer')->with('danger',$message);
        }
    }

    public function checkRewardCriteria(){

//         Define all your logics


//        if($user->reward_point_transac==50 && $user->reward_point_referal==50 && $user->no_of_referal==50):
//            $user->main_balance += 300;
////            throw new \Exception('The Fund Could not be transfered');
//        endif;

    }

	public function store()
	{
		//
	}

    public function test()
    {
        $user=Auth::user();
//        $reward=new reward();
//        $users=User::with('userReward')->find(3);
        $reward=reward::where('user_type',$user->type)->where('reward_level',$user->reward_level+1)->first();
//        dd($reward->reward_level);
//        dd(reward::where('user_type',$user->type)->where('reward_level',$user->reward_level+1)->get());
//        dd(reward::where('user_type',$user->type)->get());
        return $reward;
    }

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
