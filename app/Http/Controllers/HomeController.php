<?php namespace App\Http\Controllers;
use App\SafalSewa\Models\Notification;
use App\User;
use App\SafalSewa\Models\Bank;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
class HomeController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	// public function __construct()
	// {
	// 	$this->middleware('auth');
	// }

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function home()
	{
		return view('index');
	}

    public function login()
    {
        return view('/auth/login');
    }

    public function userInfo(){
        return view('/auth/userdetail');
    }

    public function loadfund()
    {
//        dd(Carbon::now()->toDateTimeString());
        $bank=Bank::all();
        return view('contents/loadFund')->with('bank',$bank);
    }

    public function fundtransfer(){
        return view('contents/fundtransfer');
    }

    public function ntcpostpaid()
    {
        return view('contents/ntc-postpaid');
    }

    public function ntcprepaid()
    {
        return view('contents/ntc-prepaid');
    }

    public function viewnotification($id){
        $notification=Notification::find($id);
        $notification->status += 1;
//        dd($notification->status);
        $notification->save();
        return redirect()->route('dashboard');
    }

}
