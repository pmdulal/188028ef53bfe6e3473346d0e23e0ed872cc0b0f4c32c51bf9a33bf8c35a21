<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\LoginFormRequest;
use App\Http\Requests\RegisterFormRequest;
use App\Http\Requests\DetailFormRequest;
use App\Http\Controllers\Controller;

use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class LoginController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function checkLogin(LoginFormRequest $request)
    {
        $email = $request->get('login_email');
        $password = $request->get('login_password');
        try{
            if (Auth::attempt(['email' => $email, 'password' => $password])){
                $user= Auth::user();
                if(is_null($user->fname))return redirect()->route('detail.info');
                $user->punch_in = Carbon::now()->format('Y-m-d H:i:s');

                $user->save();
                if($user->no_of_referal==100):
                    $user -> main_balance += 300;
                endif;
                return redirect()->route('dashboard');
            }
        }catch(\Exception $ex){
            $message=$ex->getMessage();
            return redirect()->route('loginhere')->with('info',$message);
        }
        return redirect()->route('loginhere')->with('warning', 'Invalid Login Credentials!');
    }

    public function doRegister(RegisterFormRequest $request)
    {
//        dd($request);
//        $email = $request->get('email');
//        $password = $request->get('password');
        $user=new User();
        $user->uname=$request->get('username');
        $user->type=$request->get('Type');
        $user->email=$request->get('Email');
        $user->source_id=$request->get('source_id');
        $user->password=Hash::make($request->get('password'));
        $user->remember_token=$request->get('_token');
        $user->save();
//        $user=new User();
        DB::table('users')->where('unique_key',$request->get('source_id'))->increment('no_of_referal');
//        dd(DB::table('users')->where('user_id',$request->get('source_id'))->first());
        if (Auth::attempt(['email' => $request->get('Email'), 'password' => $request->get('password')])){
            $user=User::find(Auth::id());
            $user->punch_in=Carbon::now()->format('Y-m-d H:i:s');
//            $user->login_time=Carbon::now()->toTimeString();
            $user->save();
            return redirect()->route('detail.info');
        }
//        return redirect()->route('detail.info');
    }

    public function insertDetail(DetailFormRequest $request){
        $user=User::find(Auth::id());
        $user->fname=$request->get('fname');
        $user->mname=$request->get('mname');
        $user->lname=$request->get('lname');
        $user->paddress=$request->get('paddress');
        $user->taddress=$request->get('taddress');
        $user->cnumber=$request->get('cnumber');
        $user->cnumber1=$request->get('cnumber1');
        $user->citizenNo=$request->get('citizenNo');
        $user->gender=$request->get('gender');
        $user->remember_token=$request->get('_token');
        $user->save();
        return redirect()->route('dashboard');

    }

    public function logOut(){
        $user = Auth::user();
        $user->punch_out = $user->punch_in;
        $user->save();
        Auth::logout();

        return redirect()->route('loginhere');
    }

    private function reCheck(Request $request)
    {
        if($request->get('password')==$request->get('password1')){
            return true;
        }
        return false;
    }



}
