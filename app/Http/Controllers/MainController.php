<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\User;
use Illuminate\Http\Request;

class MainController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function paras()
	{
        $name='paras';
		// return view('paras')->with('name','Guest');
		return view('paras')->with('name',$name);
	}

    public function action()
    {
        // return view('paras')->with('name','Guest');
        return view('action')->with('name','Guest');
    }

    public function store()
    {
//        $users=DB::table('user_info')->get();
        $User=new User();
        $users=$User::all();
        return $users;
    }

}
