<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class LoadFundFormRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
            "account_number" => "required|min:10|max:15",
            "amount" => "required|numeric|min:4",
//            "password" => "required|alpha|min:5"
		];
	}

}
