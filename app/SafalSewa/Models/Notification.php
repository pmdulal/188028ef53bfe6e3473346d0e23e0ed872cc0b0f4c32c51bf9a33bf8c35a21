<?php namespace App\SafalSewa\Models;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model {

    protected $table = 'notifications';
    protected $fillable = ['description', 'status','user_id'];

//    protected $hidden = ['password', 'remember_token'];
    public function user()
    {
        return $this->belongsTo('App\SafalSewa\Models\Notifications','id','user_id');
    }
}
