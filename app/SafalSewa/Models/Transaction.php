<?php namespace App\SafalSewa\Models;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model {

    protected $table = 'transactions';
    protected $fillable = ['ac_number','number','amount','type','bank','user_id'];

}
