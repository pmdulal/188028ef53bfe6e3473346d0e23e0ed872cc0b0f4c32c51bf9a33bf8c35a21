@extends('layout.dashboard')
@section('content')
{{--<h1>aaa</h1>--}}


            <div class="container-fluid">
            	{{--<div class="row well">--}}
            		{{--<div class="col-xs-6 col-sm-3">--}}
            			{{--<label>Main Balance</label> <span class="badge fa fa-rupee"> 562</span>--}}
            		{{--</div>--}}
            		{{--<div class="col-xs-6 col-sm-3">--}}
            			{{--<label>Reward Point</label> <span class="badge">32</span>--}}
            		{{--</div>--}}
            		{{--<div class="col-xs-6 col-sm-3">--}}
            			{{--<label>No of Referal</label> <span class="badge">562</span>--}}
            		{{--</div>--}}
            		{{--<div class="col-xs-6 col-sm-3">--}}
            			{{--<label>Last login</label> <span class="badge">2015 June 25</span>--}}
            		{{--</div>--}}
            	{{--</div>--}}
                <!-- Page Heading -->

                {{--<div class="row">--}}
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Dashboard <small>Statistics Overview</small>
                        </h1>
                        <ol class="breadcrumb">
                            <li class="active">
                                <i class="fa fa-dashboard"></i> Dashboard
                            </li>
                        </ol>
                    </div>
                {{--</div>--}}
                <!-- /.row -->
                {{--<div class="row">--}}
	                <div class="col-lg-3 col-md-6">
	                    <div class="panel panel-primary">
	                        <div class="panel-heading">
	                            <div class="row">
	                                <div class="col-xs-3">
	                                    <i class="fa fa-rupee fa-5x"></i>
	                                </div>
	                                <div class="col-xs-9 text-right">
	                                    <div>Main Balance</div>
	                                    <div class="huge">{{Auth::user()->main_balance}}</div>
	                                </div>
	                            </div>
	                        </div>
	                        <a href="#">
	                            <div class="panel-footer">
	                                <span class="pull-left">View Details</span>
	                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
	                                <div class="clearfix"></div>
	                            </div>
	                        </a>
	                    </div>
	                </div>

	                <div class="col-lg-3 col-md-6">
	                    <div class="panel panel-primary">
	                        <div class="panel-heading">
	                            <div class="row">
	                                <div class="col-xs-3">
	                                    <i class="fa fa-circle fa-5x"></i>
	                                </div>
	                                <div class="col-xs-9 text-right">
	                                    <div>Reward Point</div>
	                                    <div class="huge">{{Auth::user()->reward_point}}</div>
	                                </div>
	                            </div>
	                        </div>
	                        <a href="#">
	                            <div class="panel-footer">
	                                <span class="pull-left">View Details</span>
	                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
	                                <div class="clearfix"></div>
	                            </div>
	                        </a>
	                    </div>
	                </div>

	                <div class="col-lg-3 col-md-6">
	                    <div class="panel panel-primary">
	                        <div class="panel-heading">
	                            <div class="row">
	                                <div class="col-xs-3">
	                                    <i class="fa fa-spinner fa-5x"></i>
	                                </div>
	                                <div class="col-xs-9 text-right">
	                                    <div>No of Referal</div>
	                                    <div class="huge">{{Auth::user()->no_of_referal}}</div>
	                                </div>
	                            </div>
	                        </div>
	                        <a href="#">
	                            <div class="panel-footer">
	                                <span class="pull-left">View Details</span>
	                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
	                                <div class="clearfix"></div>
	                            </div>
	                        </a>
	                    </div>
	                </div>

	                <div class="col-lg-3 col-md-6">
	                    <div class="panel panel-primary">
	                        <div class="panel-heading">
	                            <div class="row">
	                                {{--<div class="col-xs-3">--}}
	                                    {{--<i class="fa fa-calendar-check-o fa-5x"></i>--}}
	                                {{--</div>--}}
	                                <div class="col-xs-12 text-right">
	                                    <div>Last Login</div>
	                                    <div class="date-box">{!! Auth::user()->lastPunchIn() !!}</div>
	                                </div>
	                            </div>
	                        </div>
	                        <a href="#">
	                            <div class="panel-footer">
	                                <span class="pull-left">View Details</span>
	                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
	                                <div class="clearfix"></div>
	                            </div>
	                        </a>
	                    </div>
	                </div>
                {{--</div>--}}

                {{--<div class="row">--}}
                	<!-- <div class="col-sm-4"> -->
                        <div class="panel panel-default">
                            <div class="panel-heading text-center">
                                <h3 class="panel-title">Powered By</h3>
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                	<div class="col-xs-6 col-md-3">
                                		<div class="panel panel-primary">
	                                		<div class="panel-heading">
	                                			<h2 class="panel-title">Nepal Telecom</h2>
	                                		</div>
	                                		<div class="panel-body">
	                                			<img class="img-responsive" src="assets/images/NT logo.jpg"/>
	                                		</div>
	                                	</div>
                                	</div>
                                	<div class="col-xs-6 col-md-3">
                                		<div class="panel panel-primary">
	                                		<div class="panel-heading">
	                                			<h2 class="panel-title">Ncell</h2>
	                                		</div>
	                                		<div class="panel-body">
	                                			<img class="img-responsive" src="assets/images/ncell.jpg"/>
	                                		</div>
	                                	</div>
                                	</div>
                                	<div class="col-xs-6 col-md-3">
                                		<div class="panel panel-primary">
	                                		<div class="panel-heading">
	                                			<h2 class="panel-title">Dish Home</h2>
	                                		</div>
	                                		<div class="panel-body">
	                                			<img class="img-responsive" src="assets/images/dishhome.png"/>
	                                		</div>
	                                	</div>
                                	</div>
                                	<div class="col-xs-6 col-md-3">
                                		<div class="panel panel-primary">
	                                		<div class="panel-heading">
	                                			<h2 class="panel-title">Nepal Investment Bank</h2>
	                                		</div>
	                                		<div class="panel-body">
	                                			<img class="img-responsive" src="assets/images/investment.jpg"/>
	                                		</div>
	                                	</div>
                                	</div>

	                                <!-- <img class="img-responsive" src="assets/images/NT logo.jpg"/>
	                                <img class="img-responsive" src="assets/images/NT logo.jpg"/>
	                                <img class="img-responsive" src="assets/images/NT logo.jpg"/> -->
                                </div>

                                <div class="row">
                                	<div class="col-xs-6 col-md-3">
                                		<div class="panel panel-primary">
	                                		<div class="panel-heading">
	                                			<h2 class="panel-title">Nepal Telecom</h2>
	                                		</div>
	                                		<div class="panel-body">
	                                			<img class="img-responsive" src="assets/images/NT logo.jpg"/>
	                                		</div>
	                                	</div>
                                	</div>
                                	<div class="col-xs-6 col-md-3">
                                		<div class="panel panel-primary">
	                                		<div class="panel-heading">
	                                			<h2 class="panel-title">Nepal Telecom</h2>
	                                		</div>
	                                		<div class="panel-body">
	                                			<img class="img-responsive" src="assets/images/NT logo.jpg"/>
	                                		</div>
	                                	</div>
                                	</div>
                                	<div class="col-xs-6 col-md-3">
                                		<div class="panel panel-primary">
	                                		<div class="panel-heading">
	                                			<h2 class="panel-title">Nepal Telecom</h2>
	                                		</div>
	                                		<div class="panel-body">
	                                			<img class="img-responsive" src="assets/images/NT logo.jpg"/>
	                                		</div>
	                                	</div>
                                	</div>
                                	<div class="col-xs-6 col-md-3">
                                		<div class="panel panel-primary">
	                                		<div class="panel-heading">
	                                			<h2 class="panel-title">Nepal Telecom</h2>
	                                		</div>
	                                		<div class="panel-body">
	                                			<img class="img-responsive" src="assets/images/NT logo.jpg"/>
	                                		</div>
	                                	</div>
                                	</div>
                                	<!-- <img class="img-responsive" src="assets/images/NT logo.jpg"/>
	                                <img class="img-responsive" src="assets/images/NT logo.jpg"/>
	                                <img class="img-responsive" src="assets/images/NT logo.jpg"/>
	                                <img class="img-responsive" src="assets/images/NT logo.jpg"/> -->
                                </div>
                            </div>
                        </div>
                    <!-- </div> -->
                {{--</div>--}}


            </div>
            <!-- /.container-fluid -->

        {{--</div>--}}
        @endsection