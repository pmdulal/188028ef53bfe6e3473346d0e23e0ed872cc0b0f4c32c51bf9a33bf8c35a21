@if(Session::has('success') || Session::has('info') || Session::has('warning') || Session::has('danger'))
    @if(Session::has('success'))
    <div class="alert alert-success alert-dismissible" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <strong>{{Session::get('success')}}</strong>
    </div>
    @endif

    @if(Session::has('danger'))
    <div class="alert alert-danger alert-dismissible" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <strong>{{Session::get('danger')}}</strong>
    </div>
    @endif

    @if(Session::has('info'))
    <div class="alert alert-info alert-dismissible" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <strong>{{Session::get('info')}}</strong>
    </div>
    @endif

    @if(Session::has('warning'))
    <div class="alert alert-warning alert-dismissible" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <strong>{{Session::get('warning')}}</strong>
    </div>
    @endif
@endif