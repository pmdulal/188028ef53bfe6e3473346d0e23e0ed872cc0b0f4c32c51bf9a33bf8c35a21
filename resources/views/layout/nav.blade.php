<?php
/**
 * Created by PhpStorm.
 * User: maxp
 * Date: 8/08/2015
 * Time: 7:50 PM
 */
 ?>

<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{route('dashboard')}}">Safal Sewa</a>
            </div>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">
                {{--<li class="dropdown">--}}
                    {{--<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-envelope"></i> <b class="caret"></b></a>--}}
                    {{--<ul class="dropdown-menu message-dropdown">--}}
                        {{--<li class="message-preview">--}}
                            {{--<a href="#">--}}
                                {{--<div class="media">--}}
                                    {{--<span class="pull-left">--}}
                                        {{--<img class="media-object" src="http://placehold.it/50x50" alt="">--}}
                                    {{--</span>--}}
                                    {{--<div class="media-body">--}}
                                        {{--<h5 class="media-heading"><strong>John Smith</strong>--}}
                                        {{--</h5>--}}
                                        {{--<p class="small text-muted"><i class="fa fa-clock-o"></i> Yesterday at 4:32 PM</p>--}}
                                        {{--<p>Lorem ipsum dolor sit amet, consectetur...</p>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</a>--}}
                        {{--</li>--}}
                        {{--<li class="message-preview">--}}
                            {{--<a href="#">--}}
                                {{--<div class="media">--}}
                                    {{--<span class="pull-left">--}}
                                        {{--<img class="media-object" src="http://placehold.it/50x50" alt="">--}}
                                    {{--</span>--}}
                                    {{--<div class="media-body">--}}
                                        {{--<h5 class="media-heading"><strong>John Smith</strong>--}}
                                        {{--</h5>--}}
                                        {{--<p class="small text-muted"><i class="fa fa-clock-o"></i> Yesterday at 4:32 PM</p>--}}
                                        {{--<p>Lorem ipsum dolor sit amet, consectetur...</p>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</a>--}}
                        {{--</li>--}}
                        {{--<li class="message-preview">--}}
                            {{--<a href="#">--}}
                                {{--<div class="media">--}}
                                    {{--<span class="pull-left">--}}
                                        {{--<img class="media-object" src="http://placehold.it/50x50" alt="">--}}
                                    {{--</span>--}}
                                    {{--<div class="media-body">--}}
                                        {{--<h5 class="media-heading"><strong>John Smith</strong>--}}
                                        {{--</h5>--}}
                                        {{--<p class="small text-muted"><i class="fa fa-clock-o"></i> Yesterday at 4:32 PM</p>--}}
                                        {{--<p>Lorem ipsum dolor sit amet, consectetur...</p>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</a>--}}
                        {{--</li>--}}
                        {{--<li class="message-footer">--}}
                            {{--<a href="#">Read All New Messages</a>--}}
                        {{--</li>--}}
                    {{--</ul>--}}
                {{--</li>--}}
                <li class="dropdown">
                    <?php $notifications = Auth::user()->notification->where('status',0); ?>
                    {{--{{dd($notifications)}}--}}
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bell"></i> <sup><span class="notification text-primary">{{$notifications->count()}}</span></sup><b class="caret"></b></a>
                    <ul class="dropdown-menu alert-dropdown">
                        @if($notifications->count()<1)
                            <li class="li"> No any new Notifications </li>
                        @else
                        @foreach($notifications as $notification)
                        <li>
                            <a href="{{route('view.notification',$notification->id)}}">{{$notification->description}}</a>
                        </li>
                        @endforeach
                        @endif
                        {{--{{DB::table('notifications')->where('user_id',Auth::id())}}--}}
                        {{--<li>--}}
                            {{--<a href="#">Alert Name <span class="label label-primary">Alert Badge</span></a>--}}
                        {{--</li>--}}
                        {{--<li>--}}
                            {{--<a href="#">Alert Name <span class="label label-success">Alert Badge</span></a>--}}
                        {{--</li>--}}
                        {{--<li>--}}
                            {{--<a href="#">Alert Name <span class="label label-info">Alert Badge</span></a>--}}
                        {{--</li>--}}
                        {{--<li>--}}
                            {{--<a href="#">Alert Name <span class="label label-warning">Alert Badge</span></a>--}}
                        {{--</li>--}}
                        {{--<li>--}}
                            {{--<a href="#">Alert Name <span class="label label-danger">Alert Badge</span></a>--}}
                        {{--</li>--}}
                        <li class="divider"></li>
                        <li>
                            <a href="#">View All</a>
                        </li>
                    </ul>
                </li>
                <li class="dropdown">
                	<div class="user">

                	</div>
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <img class="user-image" src="{{url('assets/images/user_image').'/'.Auth::user()->ppurl}}">
                     {{--{{$user->fname}} {{$user->mname}} {{$user->lname}} --}}
                     {{ucfirst(Auth::user()->fname)}} {{Auth::user()->mname}} {{ucfirst(Auth::user()->lname)}}
                     <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="#"><i class="fa fa-fw fa-user"></i> Profile</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-fw fa-envelope"></i> Account Detail</a>
                        </li>
                        <li>
                            <a href="{{route('load.fund')}}"><i class="fa fa-fw fa-gear"></i> Load Fund</a>
                        </li>
                        <li>
                            <a href="{{route('fund.transfer')}}"><i class="fa fa-fw fa-gear"></i>Fund Transfer</a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="{{route('logout')}}"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">

                <!-- <div class="side-nav"> -->
                    <li class="active">
                        @if(Route::currentRouteName()==='ntc.postpaid')
                        <a href="javascript:;" data-toggle="collapse" aria-expanded="true" data-target="#demo">
                        @else
                        <a href="javascript:;" data-toggle="collapse" data-target="#demo">
                        @endif
                        <i class="fa fa-fw fa-arrows-v"></i> NTC <i class="fa fa-fw fa-caret-down"></i></a>
                        @if(Route::currentRouteName()==='ntc.postpaid')
                        <ul id="demo" aria-expanded="true" class="collapse in">
                        @else
                        <ul id="demo" aria-expanded="true" class="collapse">
                        @endif
                            <li>
                                <a href="{{route('ntc.postpaid')}}" onclick="" class="active">Postpaid</a>
                            </li>
                            <li>
                                <a href="{{route('ntc.prepaid')}}">Prepaid</a>
                            </li>
                            <li>
                                <a href="#">Adsl</a>
                            </li>
                            <li>
                                <a href="#">Landline</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#dem"><i class="fa fa-fw fa-bar-chart-o"></i> NCELL <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="dem" class="collapse">
                            <li>
                                <a href="#">Postpaid</a>
                            </li>
                            <li>
                                <a href="#">Prepaid</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#de"><i class="fa fa-fw fa-table"></i> Dish Home <i class="fa fa-fw fa-caret-down"></i></a>
                        <!-- <ul id="de" class="collapse">
                            <li>
                                <a href="#">Dropdown Item</a>
                            </li>
                            <li>
                                <a href="#">Dropdown Item</a>
                            </li>
                        </ul> -->
                    </li>

                    <!-- </div> -->
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>
