<?php
/**
 * Created by PhpStorm.
 * User: maxp
 * Date: 12/08/2015
 * Time: 2:34 PM
 */
 ?>
{{--<div class="row well">--}}
    <div class="col-xs-6 col-sm-3">
        <h5>Main Balance</h5><span class="label label-info"><i class="fa fa-rupee"></i> {{Auth::user()->main_balance}}</span>
    </div>
    <div class="col-xs-6 col-sm-3">
        <h5>Reward Point</h5> <span class="label label-primary">{{Auth::user()->reward_point_transaction}} | {{Auth::user()->reward_point_transfer}}</span>
    </div>
    <div class="col-xs-6 col-sm-3">
        <h5>No of Referal</h5> <span class="label label-warning">{{Auth::user()->no_of_referal}}</span>
    </div>
    <div class="col-xs-6 col-sm-3">
        <h5>Last login</h5> <span class="label label-danger">{!! Auth::user()->lastPunchIn() !!}</span>
    </div>
{{--</div>--}}