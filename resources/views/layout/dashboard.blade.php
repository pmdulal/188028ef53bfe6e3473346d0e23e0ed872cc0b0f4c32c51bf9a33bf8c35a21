<?php
/**
 * Created by
 * PhpStorm.
 * User: maxp
 * Date: 8/08/2015
 * Time: 10:11 PM
 */
 ?>

<!DOCTYPE html>
<html>
<head>
	<title>haina thageko</title>
	<link rel="stylesheet" type="text/css" href="{{url('/assets/css/bootstrap.min.css')}}">
	<link rel="stylesheet" type="text/css" href="../assets/css/sb-admin.css">
	<link rel="stylesheet" type="text/css" href="../assets/css/style.css">
	<link rel="stylesheet" type="text/css" href="../assets/font-awesome/css/font-awesome.min.css"/>
	{{--<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">--}}

</head>
 <body>

  <div id="wrapper">
      @include('layout.nav')
      <div id="page-wrapper">
      @if(Route::currentRouteName()!='dashboard')
      @include('layout.detailinfo');
      @endif
      @include('layout.sessionMessages')
      @yield('content')
      </div>
  </div>

     <script src="{{url('/')}}/assets/js/jquery.js"></script>
     <script src="{{url('/')}}/assets/js/bootstrap.min.js"></script>
     <script src="{{url('/')}}/assets/js/jquery.easing.min.js"></script>
     <script src="{{url('/')}}/assets/js/proto.js"></script>

 </body>
 </html>
