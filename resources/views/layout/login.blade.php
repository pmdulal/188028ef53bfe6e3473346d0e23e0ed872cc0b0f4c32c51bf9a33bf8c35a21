<!DOCTYPE html>
<html>
<head>
	<title>haina thageko</title>
	<link rel="stylesheet" type="text/css" href="../assets/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="../assets/css/grayscale.css">
	<link rel="stylesheet" type="text/css" href="../assets/css/style.css">
	<link rel="stylesheet" type="text/css" href="../assets/css/jquery-ui.css">
	<link rel="stylesheet" type="text/css" href="../assets/font-awesome/css/font-awesome.min.css"/>
	{{--<script src="//code.jquery.com/jquery-1.10.2.js"></script>--}}
    <style>
        .alert{
        position: fixed;
            top: 55px;
            width:100%;
            z-index:9999;
        }
        </style>
</head>
<body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">
{{--<div class="container">--}}
    @include('layout.sessionMessages')
    @yield('login-content')
    {{--</div>--}}
    @yield('dashboard-layout')


    <script src="{{url('/')}}/assets/js/jquery.js"></script>
    <script src="{{url('/')}}/assets/js/bootstrap.min.js"></script>
    <script src="{{url('/')}}/assets/js/grayscale.js"></script>
    <script src="{{url('/')}}/assets/js/jquery.easing.min.js"></script>
    <script src="{{url('/')}}/assets/js/jquery-ui.min.js"></script>
              <script>
                $(function() {
                  $( "#radio" ).buttonset();
                });
                </script>

</body>
</html>