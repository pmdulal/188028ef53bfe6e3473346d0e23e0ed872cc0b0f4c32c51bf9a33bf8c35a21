@extends('layout.login')
@section('login-content')
<nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
     <div class="container">
         <div class="navbar-header">
             <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-main-collapse">
                 <i class="fa fa-bars"></i>
             </button>
             <a class="navbar-brand page-scroll" href="#page-top">
                  Safal <span class="light">Sewa</span>
             </a>
         </div>

         <div class="collapse navbar-collapse navbar-right navbar-main-collapse">
             <ul class="nav navbar-nav">
                 <li class="hidden">
                     <a href="#page-top"></a>
                 </li>
                 <li>
                     <a class="page-scroll" href="#">About Us</a>
                 </li>
                 <li>
                     <a class="page-scroll" href="#">FAQ's</a>
                 </li>
                 <li>
                     <a class="page-scroll" href="#">Contact Us</a>
                 </li>
             </ul>
         </div>
     </div>
 </nav>
<header class="intro">
    <div class="intro-body">
        {{--<div class="row">--}}
        <div class="alert alert-danger alert-dismissable col-md-6 col-md-offset-3 hidden">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-ban"></i> Alert!</h4>
            Danger alert preview. This alert is dismissable. A wonderful serenity has taken possession of my entire soul, like these sweet mornings of spring which I enjoy with my whole heart.
          </div>
            <div class="col-md-8 col-md-offset-2">
                <h1 class="brand-heading">Safal Sewa</h1>
                <p class="intro-text">A best way for payment gateway<br>Join Us Today</p>
                <a href="#login" class="btn btn-circle page-scroll">
                    <i class="fa fa-angle-double-down animated"> Login</i>
                </a>
                <a href="#register" class="btn btn-circle page-scroll">
                    <i class="fa fa-angle-double-down animated"> Register</i>
                </a>
            </div>
        {{--</div>--}}
    </div>
</header>
<section id="login" class="container content-section">
    {{--<div class="row">--}}
        <div class="col-md-5 col-md-offset-3">
        <h3 class="text-center">Login</h3>
            <form action="{{route('doLogin')}}" method="post" role="form">
            <input type="hidden" name="_token" value="{{csrf_token()}}">
                <div class="form-group has-feedback">
                    <label>Enter Email</label>
                    <input type="email" placeholder="Login Email" name="login_email" class="form-control">
                    <span class="fa fa-envelope form-control-feedback input-group-addon"></i></span>
                    <p class="text-danger">{{$errors->first('login_email')}}</p>
                </div>
                <div class="form-group has-feedback">
                    <label>Enter Password</label>
                    <input type="password" placeholder="Login Password" name="login_password" class="form-control">
                    <span class="fa fa-lock form-control-feedback input-group-addon"></i></span>
                    <p class="text-danger">{{$errors->first('login_password')}}</p>
                </div>

                <button type="submit" class="btn btn-default btn-block">Login</button>
                <p class="login-option below text-center">Forgot Password <span><a href="">Click here</a></span></p>
                <p class="login-option text-center">OR</p>
                <p class="login-option text-center">Create new password <span><a href="#register" class="page-scroll">Click here</a></p>
            </form>
        </div>
    {{--</div>--}}
</section>
<section id="register" class="content-section">
    {{--<div class="row">--}}
        <div class="col-lg-5 col-lg-offset-3">
        <h3 class="text-center">Register</h3>
            <form action="{{route('doRegister')}}" method="post" role="form">
             <input type="hidden" name="_token" value="{{csrf_token()}}">
                <div class="form-group">
                    <label>Enter Username</label>
                    <input type="text" placeholder="Username" name="username" class="form-control">
                    <p class="text-danger">{{$errors->first('username')}}</p>
                </div>
                {{--<div class="form-group">--}}
                    {{--<label>Select User Type</label>--}}
                    {{--<select class="form-control" name="Type">--}}
                        {{--<option>Normal</option>--}}
                        {{--<option>Retailer</option>--}}
                        {{--<option>Zoner</option>--}}
                        {{--<option>Organization</option>--}}
                    {{--</select>--}}
                {{--</div>--}}
                <div class="form-group">
                <label>Select User Type</label>
                <div id="radio" class="ui-buttonset">
                        <input type="radio" id="radio1" name="Type" value="Normal" class="ui-helper-hidden-accessible">
                            <label for="radio1" class="ui-button ui-widget ui-state-default ui-button-text-only ui-corner-left" role="button">
                                <span class="ui-button-text">Normal</span>
                            </label>
                        <input type="radio" id="radio2" name="Type" value="Retailer" checked="checked" class="ui-helper-hidden-accessible">
                            <label for="radio2" class="ui-state-active ui-button ui-widget ui-state-default ui-button-text-only" role="button">
                                <span class="ui-button-text">Retailer</span>
                            </label>
                        <input type="radio" id="radio3" name="Type" value="Zoner" class="ui-helper-hidden-accessible">
                            <label for="radio3" class="ui-button ui-widget ui-state-default ui-button-text-only ui-corner-right" role="button">
                                <span class="ui-button-text">Zoner</span>
                            </label>
                        <input type="radio" id="radio4" name="Type" value="Organization" class="ui-helper-hidden-accessible">
                            <label for="radio4" class="ui-button ui-widget ui-state-default ui-button-text-only ui-corner-right" role="button">
                                <span class="ui-button-text">Organization</span>
                            </label>
                    </div>
                </div>
                <div class="form-group">
                    <label>Enter Email Address</label>
                    <input type="email" placeholder="Email" name="Email" class="form-control">
                    <p class="text-danger">{{$errors->first('Email')}}</p>
                </div>
                <div class="form-group">
                    <label>Enter source Id</label>
                    <input type="text" placeholder="Source Id" name="source_id" class="form-control">
                    <p class="text-danger">{{$errors->first('source_id')}}</p>
                </div>
                {{--<div class="form-group">--}}
                    {{--<div class="row">--}}
                         {{--<div class="col-md-6">--}}
                             {{--<label>Enter Password</label>--}}
                             {{--<input type="password" placeholder="Password" name="password" class="form-control">--}}
                             {{--<p class="text-danger">{{$errors->first('password')}}</p>--}}
                         {{--</div>--}}
                         {{--<div class="col-md-6">--}}
                             {{--<label>Retype Password</label>--}}
                             {{--<input type="password" placeholder="Re Enter Password" name="password_confirmation" class="form-control">--}}
                             {{--<p class="text-danger">{{$errors->first('password_confirmation')}}</p>--}}
                         {{--</div>--}}
                     {{--</div>--}}
                {{--</div>--}}
                <div class="form-group">
                    <label>Enter Password</label>
                    <input type="password" placeholder="Password" name="password" class="form-control">
                    <p class="text-danger">{{$errors->first('password')}}</p>
                </div>
                <div class="form-group">
                    <label>Retype Password</label>
                    <input type="password" placeholder="Re Enter Password" name="password_confirmation" class="form-control">
                    <p class="text-danger">{{$errors->first('password_confirmation')}}</p>
                </div>
                <button type="submit" name="submit" class="btn btn-default btn-block">Register</button>
                <p class="login-option below text-center">Already have account <span><a href="#login" class="page-scroll">Sign up</a></p>
            </form>
        </div>
    {{--</div>--}}
</section>
{{--<div id="map"></div>--}}
<footer>
    <div class="text-center">
        <p>Copyright &copy; Your Website 2014</p>
    </div>
</footer>
@endsection