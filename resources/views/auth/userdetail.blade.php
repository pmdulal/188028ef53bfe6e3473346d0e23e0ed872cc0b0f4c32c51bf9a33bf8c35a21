<?php
/**
 * Created by PhpStorm.
 * User: maxp
 * Date: 9/08/2015
 * Time: 12:58 PM
 */
 ?>
 <!doctype html>
 <html lang="en">
 <head>
 <meta charset="UTF-8">
 <title>Document</title>
    <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css">
 	<!-- <link rel="stylesheet" type="text/css" href="css/grayscale.css"> -->
    <link rel="stylesheet" type="text/css" href="font-awesome/css/font-awesome.min.css">
     <link rel="stylesheet" type="text/css" href="assets/css/style.css">
     <link rel="stylesheet" type="text/css" href="assets/css/sb-admin.css">
     <link rel="stylesheet" type="text/css" href="assets/css/basic.css">
 	<link rel="stylesheet" type="text/css" href="assets/css/dropzone.css">
 	<link rel="stylesheet" type="text/css" href="assets/css/style.css">
 </head>
<body>
 <div class="container-fluid">
     <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
         <!-- <div class="container"> -->
             <div class="navbar-header">

                 <a class="navbar-brand" href="index.html">Safal Sewa</a>
             </div>
             <ul class="nav navbar-right top-nav">
                 <li class="dropdown">
                     <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                     <img class="user-image" src="{{url('assets/images/user_image/').'/'.Auth::user()->ppurl}}">
                      Paras Dulal <b class="caret"></b></a>
                     <ul class="dropdown-menu">
                         <li>
                             <a href="#"><i class="fa fa-fw fa-user"></i> Profile</a>
                         </li>
                         <li>
                             <a href="#"><i class="fa fa-fw fa-envelope"></i> Inbox</a>
                         </li>
                         <li>
                             <a href="#"><i class="fa fa-fw fa-gear"></i> Settings</a>
                         </li>
                         <li class="divider"></li>
                         <li>
                             <a href="#"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                         </li>
                     </ul>
                 </li>
             </ul>


     </nav>
     <div class="">
         <div class="well">
             <h1 class="lead">Please Enter Your Details to Proceed!!</h1>
         </div>
     </div>
     <div class="row">
         <div class="col-md-7 col-md-offset-2">
             <form role="form" action="{{route('info')}}" method="post">
             <input type="hidden" name="_token" value="{{csrf_token()}}">
                 <div class="form-group">
                     <label>Enter Full Name</label>
                     <div class="row">
                         <div class="col-md-5">
                             <input type="text" placeholder="First Name" name="fname" class="form-control">
                             <p class="help-block">{{$errors->first('fname')}}</p>
                         </div>
                         <div class="col-md-3">
                             <input type="text" placeholder="Middle Name" name="mname" class="form-control">
                             <p class="help-block"></p>
                         </div>
                         <div class="col-md-4">
                             <input type="text" placeholder="Last Name" name="lname" class="form-control">
                             <p class="help-block">{{$errors->first('lname')}}</p>
                         </div>
                     </div>
                 </div>

                 <div class="form-group">
                     <label>Enter Address</label>
                     <div class="row">
                         <div class="col-md-6">
                             <input type="text" placeholder="Permanent Address" name="paddress" class="form-control">
                             <p class="help-block">{{$errors->first('paddress')}}</p>
                         </div>
                         <div class="col-md-6">
                             <input type="text" placeholder="Temporary Address" name="taddress" class="form-control">
                             <p class="help-block"></p>
                         </div>
                     </div>
                 </div>

                 <div class="form-group">
                     <label>Enter Contact Number</label>
                     <div class="row">
                         <div class="col-md-6">
                             <input type="text" placeholder="Contact Number" name="cnumber" class="form-control">
                             <p class="help-block">{{$errors->first('cnumber')}}</p>
                         </div>
                         <div class="col-md-6">
                             <input type="text" placeholder="Optional Number (if any)" name="cnumber1" class="form-control">
                             <p class="help-block"></p>
                         </div>
                     </div>
                 </div>

                 <div class="form-group">
                     <div class="row">
                         <div class="col-md-3">
                         <label>Select gender</label>
                             <div class="radio">
                                 <label><input type="radio" name="gender" value="m">Male</label>
                             </div>
                             <div class="radio">
                                 <label><input type="radio" name="gender" value="f">Female</label>
                             </div>
                         </div>
                         <div class="col-md-4">
                         <label>Enter ID number</label>
                             <input type="text" placeholder="CitizenShip Number" name="citizenNo" class="form-control">
                             <p class="help-block">{{$errors->first('citizenNo')}}</p>
                         </div>
                         <div class="col-md-5">
                             <div class="row">
                                 <label>User Image</label>
                             </div>
                             <div class="row">
                                 <img class="img-thumbnail" src="assets/images/blank-user.jpg">
                             </div>


                         </div>
                     </div>
                 </div>
                <div class="form-group">
                <div class="image_upload_div dropzone"></div>
                </div>
                <div class="form-group">
                    <button class="btn btn-primary" type="submit" name="submit">Submit</button>
                </div>

                 {{--<div class="col-md-6">--}}
                     {{--<div class="row">--}}
                         {{--<div class="form-group">--}}
                             {{--<label>Choose Photo</label>--}}
                             {{--<span class="btn btn-default btn-file">Browse<input type="file"></span>--}}
                         {{--</div>--}}
                     {{--</div>--}}
                 {{--</div>--}}

             </form>

             {{--<form action="/upload" class="dropzone dz-clickable" id="demo-upload" role="form">--}}
               {{--<div class="dz-message">--}}
                 {{--Drop files here or click to upload.<br>--}}
                 {{--<span class="note">(This is just a demo dropzone. Selected files are <strong>not</strong> actually uploaded.)</span>--}}
               {{--</div>--}}



             {{--</form>--}}

         </div>
     </div>


     </div>
     <script src="assets/js/jquery.js"></script>
     <script src="assets/js/bootstrap.min.js"></script>
     <script src="assets/js/dropzone.js"></script>
     <script>
     	var myDropzone = new Dropzone(".image_upload_div",
     	    {
                url: "{{route('image.upload')}}" ,
                addRemoveLinks: true,
                maxFiles: 1,
                maxfilesreached: function(file) {

                            console.log('maxfilesreached');

                        }
     	    }
     	)




     	myDropzone.on("sending", function(file, xhr, formData) {
                  // Will send the filesize along with the file as POST data.
                  formData.append("_token", $('[name=_token]').val());
                  console.log(xhr.responseText);
                });
     </script>
     <!-- // <script src="js/grayscale.js"></script> -->
     <!-- // <script src="js/jquery.easing.min.js"></script> -->
 </body>
 </html>