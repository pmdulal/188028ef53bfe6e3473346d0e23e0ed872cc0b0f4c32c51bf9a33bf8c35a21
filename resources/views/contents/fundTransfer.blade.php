<?php
/**
 * Created by PhpStorm.
 * User: maxp
 * Date: 14/08/2015
 * Time: 5:53 PM
 */
 ?>

  @extends('layout.dashboard')
  @section('content')
  <div id="page-wrapper">

      <div class="container-fluid">
        {{--<div class="row well">--}}
            {{--<div class="col-xs-6 col-sm-3">--}}
                {{--<label>Main Balance</label> <span class="badge fa fa-rupee"> {{Auth::user()->main_balance}}</span>--}}
            {{--</div>--}}
            {{--<div class="col-xs-6 col-sm-3">--}}
                {{--<label>Reward Point</label> <span class="badge">{{Auth::user()->reward_point}}</span>--}}
            {{--</div>--}}
            {{--<div class="col-xs-6 col-sm-3">--}}
                {{--<label>No of Referal</label> <span class="badge">{{Auth::user()->no_of_referal}}</span>--}}
            {{--</div>--}}
            {{--<div class="col-xs-6 col-sm-3">--}}
                {{--<label>Last login</label> <span class="badge">{{Auth::user()->last_login_date }}&nbsp;&nbsp;&nbsp;{{Auth::user()->last_login_time}}</span>--}}
            {{--</div>--}}
        {{--</div>--}}
        {{--@include('layout.detailinfo');--}}
          <!-- Page Heading -->
          {{--<div class="row">--}}
              <div class="col-lg-12">
                  <h1 class="page-header">
                      Nepal Telecom <small>Postpaid</small>
                  </h1>
                  <ol class="breadcrumb">
                      <li class="active">
                          <i class="fa fa-dashboard"></i> Dashboard
                      </li>
                  </ol>
              </div>
          {{--</div>--}}

          {{--<div class="row">--}}
            <div class="col-md-4 col-md-offset-2">
                <form role="form" action="{{route('transfer.fund')}}" method="post">
                 <input type="hidden" name="_token" value="{{csrf_token()}}">

                    <div class="form-group">
                    <label>Enter recipient Id</label>
                        <!-- <div class="row"> -->
                            <input type="text" placeholder="Safal Sewa Id" name="Safal_Sewa_Id" class="form-control">
                            <p class="text-danger">{{$errors->first('Safal_Sewa_Id')}}</p>
                        <!-- </div> -->
                    </div>
                      <div class="form-group">
                      <label>Enter Amount to Transfer</label>
                          <!-- <div class="row"> -->
                              <input type="text" placeholder="Amount" name="amount" class="form-control">
                              <p class="text-danger">{{$errors->first('amount')}}</p>
                          <!-- </div> -->
                      </div>

                      <div class="form-group">
                          <!-- <div class="row"> -->
                              <button class="btn btn-primary btn-block" type="submit" name="submit">Submit</button>
                          <!-- </div> -->
                      </div>
                </form>
            </div>
        {{--</div>--}}
        {{--end row--}}
      </div>
  </div>
  @endsection