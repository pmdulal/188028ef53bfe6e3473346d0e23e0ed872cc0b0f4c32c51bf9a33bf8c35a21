<?php
/**
 * Created by PhpStorm.
 * User: maxp
 * Date: 8/08/2015
 * Time: 11:36 PM
 */
 ?>

 @extends('layout.dashboard')
  @section('content')
  <div id="page-wrapper">

              <div class="container-fluid">
              	{{--<div class="row well">--}}
              		{{--<div class="col-xs-6 col-sm-3">--}}
              			{{--<label>Main Balance</label> <span class="badge fa fa-rupee"> 562</span>--}}
              		{{--</div>--}}
              		{{--<div class="col-xs-6 col-sm-3">--}}
              			{{--<label>Reward Point</label> <span class="badge">32</span>--}}
              		{{--</div>--}}
              		{{--<div class="col-xs-6 col-sm-3">--}}
              			{{--<label>No of Referal</label> <span class="badge">562</span>--}}
              		{{--</div>--}}
              		{{--<div class="col-xs-6 col-sm-3">--}}
              			{{--<label>Last login</label> <span class="badge">2015 June 25</span>--}}
              		{{--</div>--}}
              	{{--</div>--}}

                  <!-- Page Heading -->
                  <div class="row">
                      <div class="col-lg-12">
                          <h1 class="page-header">
                              Nepal Telecom <small>Prepaid</small>
                          </h1>
                          <ol class="breadcrumb">
                              <li class="active">
                                  <i class="fa fa-dashboard"></i> Dashboard
                              </li>
                          </ol>
                      </div>
                  </div>

                  <div class="row">
  		        	<div class="col-md-4 col-md-offset-2">
  		        		<form role="form">
  		        			<div class="form-group">
  		                    <label>Enter Your Postpaid Number</label>
  			                    <!-- <div class="row"> -->
  		                            <input type="text" placeholder="Number" name="number" class="form-control">
  		                            <p class="help-block">help text here.</p>
  			                    <!-- </div> -->
  			                </div>
                              <div class="form-group">
                              <label>Enter Amount to Transfer</label>
                                  <!-- <div class="row"> -->
                                      <input type="text" placeholder="Amount" name="amount" class="form-control">
                                      <p class="help-block">help text here.</p>
                                  <!-- </div> -->
                              </div>
                              <div class="form-group">
                                  <!-- <div class="row"> -->
                                      <button class="btn btn-primary btn-block" type="submit" name="submit">Submit</button>
                                  <!-- </div> -->
                              </div>
  		        		</form>
  		        	</div>
          		</div>
              </div>
          </div>
          @endsection
