<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('transactions', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('ac_number',25)->nullable();
			$table->string('number',20)->nullable();
			$table->string('amount',20);
			$table->string('id_to',20);
			$table->string('type',20);
			$table->string('bank',20)->nullable();
			$table->bigInteger('user_id')->unsigned();
			$table->timestamps();

            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onUpdate('cascade')
                ->onDelete('restrict');
		});


	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('transactions', function ($table) {
            $table->dropForeign('transactions_user_id_foreign');
        });

		Schema::drop('transactions');
	}

}
