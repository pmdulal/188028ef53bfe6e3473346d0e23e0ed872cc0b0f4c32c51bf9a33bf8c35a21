<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{
			$table->bigIncrements('id');
			$table->string('unique_key')->nullable();
			$table->string('fname')->nullable();
			$table->string('mname')->nullable();
			$table->string('lname')->nullable();
			$table->string('email')->unique();
			$table->string('paddress')->nullable();
			$table->string('taddress')->nullable();
			$table->string('cnumber')->nullable();
			$table->string('cnumber1')->nullable();
			$table->string('citizenNo')->nullable();
			$table->string('gender')->nullable();
			$table->string('ppurl')->nullable();
			$table->string('ctizenurl')->nullable();
			$table->string('uname')->unique();
			$table->string('type');
			$table->string('password');
			$table->integer('main_balance');
			$table->integer('reward_point_transaction');
			$table->integer('reward_point_transfer');
			$table->integer('no_of_referal');
			$table->datetime('punch_in');
			$table->datetime('punch_out');
			$table->string('source_id', 10)->nullable();
			$table->integer('total_transaction')->nullable();
			$table->integer('total_transfer')->nullable();
			$table->rememberToken();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
