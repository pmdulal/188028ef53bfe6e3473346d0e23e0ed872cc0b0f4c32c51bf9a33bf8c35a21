<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserRewardsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('user_rewards', function(Blueprint $table)
		{
            $table->increments('id');
            $table->integer('reward_level')->nullable();
            $table->integer('transaction_point');
            $table->integer('referral_number');
            $table->integer('transfer_point');
            $table->integer('reward');
            $table->bigInteger('user_id')->unsigned();
            $table->timestamps();

            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onUpdate('cascade')
                ->onDelete('restrict');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('user_rewards', function ($table) {
            $table->dropForeign('user_rewards_user_id_foreign');
        });

		Schema::drop('user_rewards');
	}

}
