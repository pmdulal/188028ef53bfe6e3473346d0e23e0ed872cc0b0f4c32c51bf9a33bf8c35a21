<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\SafalSewa\Models\Bank;
use App\SafalSewa\Models\reward;

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();
        $data=[
            "Nepal Investment Bank",
            "Prabhu Bank",
            "Rastria Banijya Bank",
            "Global Bank"
        ];

        foreach($data as $name){
            Bank::create(['name'=>$name]);
        }
        $this->call('RewardsTableSeeder');


	}

}

class RewardsTableSeeder extends Seeder {

    public function run()
    {
        DB::table('rewards')->delete();

        $rewards = [
            [
                'reward_level' => 1 ,
                'transaction_point' => 50 ,
                'referral_number' => 100 ,
                'transfer_point' => 30 ,
                'reward' => 300 ,
                'user_type' => 'normal' ,
            ],

            [
                'reward_level' => 1 ,
                'transaction_point' => 500 ,
                'referral_number' => 100 ,
                'transfer_point' => 0 ,
                'reward' => 300 ,
                'user_type' => 'retailer' ,
            ],

            [
                'reward_level' => 1 ,
                'transaction_point' => 400 ,
                'referral_number' => 100 ,
                'transfer_point' => 200 ,
                'reward' => 300 ,
                'user_type' => 'zone' ,
            ],

            [
                'reward_level' => 1 ,
                'transaction_point' => 50 ,
                'referral_number' => 200 ,
                'transfer_point' => 50 ,
                'reward' => 300 ,
                'user_type' => 'organisation' ,
            ],
//            end level 1
            [
                'reward_level' => 2 ,
                'transaction_point' => 100 ,
                'referral_number' => 200 ,
                'transfer_point' => 100 ,
                'reward' => 500 ,
                'user_type' => 'normal' ,
            ],

            [
                'reward_level' => 2 ,
                'transaction_point' => 700 ,
                'referral_number' => 200 ,
                'transfer_point' => 0 ,
                'reward' => 500 ,
                'user_type' => 'retailer' ,
            ],

            [
                'reward_level' => 2 ,
                'transaction_point' => 550 ,
                'referral_number' => 200 ,
                'transfer_point' => 300 ,
                'reward' => 500 ,
                'user_type' => 'zone' ,
            ],

            [
                'reward_level' => 2 ,
                'transaction_point' => 75 ,
                'referral_number' => 400 ,
                'transfer_point' => 75 ,
                'reward' => 500 ,
                'user_type' => 'organisation' ,
            ],
//            end level 2
            [
                'reward_level' => 3 ,
                'transaction_point' => 200 ,
                'referral_number' => 300 ,
                'transfer_point' => 200 ,
                'reward' => 1000 ,
                'user_type' => 'normal' ,
            ],

            [
                'reward_level' => 3 ,
                'transaction_point' => 100 ,
                'referral_number' => 300 ,
                'transfer_point' => 0 ,
                'reward' => 1000 ,
                'user_type' => 'retailer' ,
            ],

            [
                'reward_level' => 3 ,
                'transaction_point' => 800 ,
                'referral_number' => 300 ,
                'transfer_point' => 500 ,
                'reward' => 1000 ,
                'user_type' => 'zone' ,
            ],

            [
                'reward_level' => 3 ,
                'transaction_point' => 100 ,
                'referral_number' => 800 ,
                'transfer_point' => 100 ,
                'reward' => 1000 ,
                'user_type' => 'organisation' ,
            ],
//            end level 3
            [
                'reward_level' => 4 ,
                'transaction_point' => 500 ,
                'referral_number' => 508 ,
                'transfer_point' => 500 ,
                'reward' => 'smart phone' ,
                'user_type' => 'normal' ,
            ],

            [
                'reward_level' => 4 ,
                'transaction_point' => 1400 ,
                'referral_number' => 505 ,
                'transfer_point' => 0 ,
                'reward' => 'smart phone' ,
                'user_type' => 'retailer' ,
            ],

            [
                'reward_level' => 4 ,
                'transaction_point' => 1000 ,
                'referral_number' => 505 ,
                'transfer_point' => 1200 ,
                'reward' => 'smart phone' ,
                'user_type' => 'zone' ,
            ],

            [
                'reward_level' => 4 ,
                'transaction_point' => 150 ,
                'referral_number' => 1200 ,
                'transfer_point' => 200 ,
                'reward' => 5000 ,
                'user_type' => 'organisation' ,
            ],
//            end level 4
            [
                'reward_level' => 5 ,
                'transaction_point' => 500 ,
                'referral_number' => 508 ,
                'transfer_point' => 500 ,
                'reward' => 'lucky draw' ,
                'user_type' => 'normal' ,
            ],

            [
                'reward_level' => 5 ,
                'transaction_point' => 2000 ,
                'referral_number' => 505 ,
                'transfer_point' => 0 ,
                'reward' => 'lucky draw' ,
                'user_type' => 'retailer' ,
            ],

            [
                'reward_level' => 5 ,
                'transaction_point' => 1000 ,
                'referral_number' => 505 ,
                'transfer_point' => 1200 ,
                'reward' => 'lucky draw' ,
                'user_type' => 'zone' ,
            ],

            [
                'reward_level' => 5 ,
                'transaction_point' => 200 ,
                'referral_number' => 2000 ,
                'transfer_point' => 400 ,
                'reward' => 'lucky draw' ,
                'user_type' => 'organisation' ,
            ],
//            end level 5
        ];
        foreach($rewards as $reward):
            reward::create($reward);
        endforeach;
    }

}
